#!/home/hdiwan/.virtualenvs/loc/bin/python
import csv
import datetime
import logging
import re
import smtplib
import subprocess
import time

from flask import abort, Flask, jsonify, make_response, render_template, request, send_from_directory, session
import pytz
import requests
import webhelpers.date

app = Flask(__name__)

@app.route('/robots.txt')
def robots():
    # blocks all bots
    return '''User-agent: *
Disallow: /
'''

def get_whois_info(address):
    ret = []
    obj = country_location(address)
    obj = '\n'.join([o for o in obj.split('\n') if not o.find(':') == -1 or not o.startswith('#')])
    return obj


def country_location(address):
    ret = subprocess.check_output(['whois', address])
    return ret

def alert():
    if request.method != 'GET':
        return
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login("hd1@jsc.d8u.us", "Wonju777")
    address = request.headers.get('X-Real-IP', 'no forwarding information found')
    address_info = get_whois_info(address)
    msg = "Subject: New Location access\n\n{0} just accessed where I was. It's whois information is:\n\n{1}".format(address, address_info)

    server.sendmail("hasandiwan+whereami@gmail.com", "hd1@jsc.d8u.us", msg)
    server.quit()


@app.route('/access', methods=['GET'])
def access_log():
    json_dict = {'locations': locations.keys()}
    return jsonify(json_dict)


@app.before_request
def csrf_protect():
    START_TIME = time.time()
    RANDOM_KEY = 'hasandiwan@gmail.com:{0}'.format(START_TIME)
    if '_csrf_token' not in session:
        session['_csrf_token'] = app.secret_key

    token = session.pop('_csrf_token', app.secret_key)
    app.jinja_env.globals['csrf_token'] = token
    if not token:
        abort(401)


@app.route('/loc/<location>', methods=['GET'])
def new_location_for_hasan_is(location):
    logging.debug(location)
    loc_parts = location.split(',')
    lat = loc_parts[0]
    lng = loc_parts[1]
    row = [str(time.time()), str(lat), str(lng)]
    with open('data.csv','w') as fout_:
        writer = csv.writer(fout_)
        logging.debug(row)
        writer.writerow(row)
    logging.debug('file closed!')
    return make_response(location)


@app.route('/', methods=['GET'])
def GET():
    point = []
    ret = make_response('', 406)
    # Create marker on map and return page
    try:
        with open('data.csv') as fin:
            reader = csv.reader(fin)
            point = list(reader)[0]
        latitude = point[1]
        longitude = point[2]
        icon = 'static/RedX4.png'
        timestamp = datetime.datetime.fromtimestamp(float(point[0]))
        ts_ = pytz.timezone('UTC').localize(timestamp).astimezone(pytz.timezone('UTC')).strftime('%c')
        ts = webhelpers.date.time_ago_in_words(datetime.datetime.strptime(ts_, '%c'))
        ret = make_response(render_template('main.tmpl', time=ts, latitude=latitude, longitude=longitude))

        # log access -- from http://www.unknownerror.org/opensource/mitsuhiko/flask/q/stackoverflow/3759981/get-ip-address-of-visitors-using-python-flask
        alert()
    except IOError, e:
        ret_ = '''<!DOCTYPE html><html><body><h1>Error</h1><p>{0}</p>'''.format(str(e))
        ret = make_response(ret_, 406)

    return ret


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='"%(created)f", "%(lineno)d", "%(message)s"')
    START_TIME = datetime.datetime.now().strftime('%s')
    app.secret_key = hash('hasan.diwan-{0}@gmail.com'.format(START_TIME))
    app.debug = False
    app.run(port=15177, host='127.0.0.1')
